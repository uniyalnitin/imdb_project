# Readme
## About
	This is imdb like application where you can see the movies and their details as well

## Running instruction
1.	using docker
	1.	run following command to build the image `docker-compose build`
    2.	run following command to start the server	`docker-compose up`
    3.	open the url `0.0.0.0:5000` locally to access the app
2. 	using python virtual environment
	1.	run following command `python3 -m venv venv`
    2.	activate the virtual environment `source ./venv/bin/activate`
    3.	install the requirements `pip3 install -r requirements.txt`
    4.	initialize the db `python3 manage.py db init`
    5.  create migrations `python3 manage.py db migrate`
    6.	upgrade the db `python3 manage.py db upgrade`
    7.  start the server `python3 manage.py run`
3. Running test cases
	1. run following command to run the test cases `python3 manage.py test`
## Routes Information
### User's routes 
1.	list all users
	1.	url:  <host:port>/user/
    2.	method: get
  	3.	headers: not required
    4.	return: List of users
2. Create new user
	1. url: <host:port>/user/
    2. method: post
    3. body: 
    	```
        {
                "email": "string", ->(required)
                "username": "string", ->(required)
                "password": "string",	-> (required)
                "is_admin": false		-> (default=False)
          }```
          
     4.	return: Newly created user and status_code
3.	Get user detail
	1. url: <host:port>/user/{id}
  	2. method: get
  	3. request params: userid
  	4. return: user or 404

### Authentication routes
1.	Login 
	1.	url: <host:port>/auth/login
  	2.	method: post
  	3. 	body: 
  		```
        {
                "email": "string", ->(required)
                "password": "string",	-> (required)
          }```
  	4.	return: `X-API-KEY` in response body (authentication token which you need to send in headers to access private routes)
 
### Movies routes
1. View all movies	(private route, need to login to access this route)
	1.	url: <host:port>/movies/
  	2.	method: get
  	3.	header:	
  		```
  		{
  			"X-API-KEY": <your_token>
  		}```
  	4.	return: List of movies or 401
  
2.	Create new movie (private route, you need to login with admin account (admin only))
	1.	url: <host:port>/movies/
  	2.	method: post
  	3. 	header: 
  		```
  		{
  			"X-API-KEY": <your_token>
  		}```
  	4. body:
  		```
  		{
          "name": "string",		--> (required)
          "imdb_score": 0,
          "genres": [
            "string"
          ],
          "director": "string"
        }```
	5. return:	Newly created object or error

3. Search movies by the name (case insensitive)
	1.	url: <host:port>/movies/search
  	2. 	method: get
  	3.	query params: name --> name or part of name (substring of movie name, case insesitive)
	4. return: list of movies
  
4.	Edit movie (private route, you need to login with admin account (admin only))
  	1.	url: <host:port>/movies/{id}
  	2.	method: put
  	3. 	request params: movie id
  	4.	header: 
  		```
  		{
  			"X-API-KEY": <your_token>
  		}```
  	5. 	body: 
  		```
  		{
          "name": "string",		--> (required)
          "imdb_score": 0,
          "genres": [
            "string"
          ],
          "director": "string"
        }```
  	6. return: Newly updated object or 404
5. Delete Movie (private route, you need to login with admin account (admin only))
	1. url: <host:port>/movies/{id}
  	2. method: delete
  	3. request params: movie id
  	4. header: 
  		```
  		{
  			"X-API-KEY": <your_token>
  		}```
  	5. return: success message or 404

6. Movie Detail 
  	1.	url: <host:port>/movies/{id}
  	2.	method: get
  	3.	return: Movie detail or 404

### Genre routes
1.	Get list of genres
	1. url: <host:port>/genres/
  	2. method: get
  	3. return: list of all genres
2.	Create genre (private route, you need to login with admin account (admin only))
  	1.	url: <host:port>/genres/
  	2.	method: post
  	3.	header: 
  		```
  		{
  			"X-API-KEY": <your_token>
  		}```
  	4. 	body: 
  		```
  		{
          "name": "string",		--> (required)
        }```
  	5.	return: newly created genre or 500
3.	Edit genre (private route, you need to login with admin account (admin only))
  	1.	url: <host:port>/genres/{id}
  	2.	method: put
  	3. 	header: 
  		```
  		{
  			"X-API-KEY": <your_token>
  		}```
  	4.	request params: genre id
  	5.	body: 
  		```
  		{
          "name": "string",		--> (required)
        }```
  	6.	return:	updated genre or 404
4.	Get all movies that has current genre
  	1.	url: <host:port>/genres/{id}/movies
  	2. 	method: get
  	3.	return: list of movies that has current genre

### Director's routes
1.	Get list of directors
	1. url: <host:port>/directors/
  	2. method: get
  	3. return: list of all directors
2.	Create director (private route, you need to login with admin account (admin only))
  	1.	url: <host:port>/directors/
  	2.	method: post
  	3.	header: 
  		```
  		{
  			"X-API-KEY": <your_token>
  		}```
  	4. 	body: 
  		```
  		{
          "name": "string",		--> (required)
        }```
  	5.	return: newly created director or 500
    
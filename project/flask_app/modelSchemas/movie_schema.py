from flask_restplus import Namespace, fields
from ..models.genre import Genre
from ..models.movie import Movie
from ..models.director import Director
from ..utils.serializers import model_serializer
from flask import jsonify


class MovieSchema:
    authorizations = {
        'apikey': {
            'type': 'apiKey',
            'in': 'header',
            'name': 'X-API-KEY'
        },
    }
    api = Namespace('movie', description="movie related operations",
                    authorizations=authorizations)

    movie_fields = {
        'name': fields.String(required=True, description="movie name"),
        'imdb_score': fields.Float(required=False, description="imdb score"),
        'genres': fields.List(fields.String(required=False, description="list of movie genre")),
        'director': fields.String(required=False, description="director")
    }

    movie = api.model('movie', movie_fields)

    @classmethod
    def format_and_save(cls, data):
        name = data['name']
        imdb_score = data.get('imdb_score', 0.0)
        genres = data.get('genres', [])

        def parse(resp, key): return resp[0][key]
        genres = [parse(Genre.get_or_create_genre(name), 'genre')
                  for name in genres]
                  
        response, status_code = Movie.create_movie(name, imdb_score)

        movie = response.get('movie', None)
        if movie:
            movie.add_genre_list(genres)

        director = None 
        if data.get('director'):
            director = parse(Director.get_or_create_director(data['director']), 'director')
            director.add_movie(movie)
            director.save_data()
        movie.save_data()
        return {
            'status': response['status'],
            'message': response['message'],
            'movie': model_serializer(movie) if movie else None
        }, status_code

    @classmethod
    def format_edit_save(cls, movie, data):
        def parse(resp, key): return resp[0][key]
        genres = [parse(Genre.get_or_create_genre(name), 'genre')
                  for name in data.get('genres', [])]
        movie.name = data['name']
        movie.imdb_score = data.get('imdb_score', 0.0)
        movie.genres = genres

        if data.get('director'):
            old_director = movie.get_director()
            if old_director:
                old_director.remove_movie(movie) # unassign the previous director
            director = parse(Director.get_or_create_director(data['director']), 'director')
            director.add_movie(movie) # assign new director
            director.save_data()
        movie.save_data()
        return {
            'status': 'success',
            'message': 'successfully updated movie',
            'movie': model_serializer(movie)
        }, 203
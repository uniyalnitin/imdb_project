from flask_restplus import Namespace, fields
from ..models.director import Director
from ..utils.serializers import model_serializer

class DirectorSchema:
    authorizations = {
        'apikey': {
            'type': 'apiKey',
            'in': 'header',
            'name': 'X-API-KEY'
        },
    }
    api = Namespace('director', description="director related operations",
                    authorizations=authorizations)

    directors = api.model('director', {
        'name': fields.String(required=True, description="director name")
    })

    @classmethod
    def format_and_save(cls, data):
        name = data['name']
        response, status_code = Director.create_director(name)
        director = response.get('director')

        return {
            'status': response['status'],
            'message': response['message'],
            'director': model_serializer(director) if director else None
        }, status_code
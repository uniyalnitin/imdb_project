from flask import request
from project.flask_app.models.user import User 

class AuthHelper:

    @staticmethod
    def login(data):
        try:
            user = User.query.filter_by(email = data.get('email')).first()
            if user and user.is_valid_password(data.get('password')):
                auth_token = user.generate_auth_token()
                if auth_token:
                    response_object = {
                        'status': 'success',
                        'message': 'Successfully logged in',
                        'X-API-KEY':  auth_token
                    }
                    return response_object, 200, {'X-API-KEY': auth_token}
            else:
                response_object = {
                    'status': 'error',
                    'message': 'Invalid email or password'
                }
                return response_object, 401

        except Exception as e:
            response_object = {
                'status': 'error',
                'message': e
            }
            return response_object, 500

    @staticmethod
    def get_logged_in_user():
        headers = request.headers
        token = request.headers.get('X-API-KEY')
        if not token:
            return None
        user_payload = User.decode_auth_token(token)
        user_id = user_payload['id']
        return User.get_user(user_id) or None
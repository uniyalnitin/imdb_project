from flask import request
from flask_restplus import Resource, fields
from project.flask_app.utils.decorators import login_required, admin_login_required
from project.flask_app.models.movie import Movie
from project.flask_app.modelSchemas.movie_schema import MovieSchema

api = MovieSchema.api
movie_schema = MovieSchema.movie


@api.route('/')
class Movies(Resource):
    @login_required
    @api.doc(description='list of all movies', security="apikey", headers={'X-API-KEY': 'please provide authentication token'})
    @api.marshal_list_with(movie_schema, envelope="data")
    def get(self):
        return Movie.get_all_movies(), 200

    @admin_login_required
    @api.doc(description='create a new movie', security='apikey', headers={'X-API-KEY': 'please provide authentication token'})
    @api.expect(movie_schema, validate=True)
    def post(self):
        data = api.payload
        response, code = MovieSchema.format_and_save(data)
        return response, code


@api.route('/<id>')
@api.doc(params={'id': 'Movie Id'})
class MovieDetail(Resource):
    @api.doc(description='movie detail', params={'id': 'Movie id'})
    @api.marshal_with(movie_schema)
    def get(self, id):
        movie = Movie.get_movie(id)
        if not movie:
            api.abort(404)
        return movie, 200

    @admin_login_required
    @api.doc(description='edit movie', security="apikey", headers={'X-API-KEY': 'please provide authentication token'})
    @api.expect(movie_schema, validate=True)
    def put(self, id):
        movie = Movie.get_movie(id)
        if not movie:
            api.abort(404)
        data = api.payload
        return MovieSchema.format_edit_save(movie, data)

    @admin_login_required
    @api.doc(description='delete movie', security="apikey", headers={'X-API-KEY': 'please provide authentication token'})
    def delete(self, id):
        movie = Movie.get_movie(id)
        if not movie:
            api.abort(404)
        movie.delete_movie()
        return {
            'status': 'success',
            'message': 'movie deleted successfully'
        }, 200


@api.route('/search')
@api.doc(description='search movies that has name in their substring (case insensitive)', params={'name': {'description': 'Movie Name substring'}})
class MovieSearch(Resource):
    @api.marshal_list_with(movie_schema, envelope="data")
    def get(self):
        name = request.args.get('name')
        movies = Movie.query.filter(Movie.name.ilike(f'%{name}%')).all()
        return movies, 200

import datetime
import jwt
from project.extensions import flask_bcrypt, db
from project.config.main import secret_key


class User(db.Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(255), unique=True, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    is_admin = db.Column(db.Boolean, nullable=False, default=False)
    username = db.Column(db.String(50), unique=True)
    password_hash = db.Column(db.String(100))

    @property
    def password(self):
        raise AttributeError('password: write-only field')

    @password.setter
    def password(self, password):
        self.password_hash = flask_bcrypt.generate_password_hash(
            password).decode('utf-8')

    def is_valid_password(self, password):
        return flask_bcrypt.check_password_hash(self.password_hash, password)

    @classmethod
    def get_all_users(cls):
        return cls.query.all()

    @classmethod
    def get_user(cls, id):
        return cls.query.filter_by(id=id).first()

    @classmethod
    def create_user(cls, data):
        user = cls.query.filter_by(email=data['email']).first()
        if not user:
            new_user = User(
                email=data['email'],
                username=data['username'],
                password=data['password'],
                is_admin=data.get('is_admin', False),
                created_at=datetime.datetime.now()
            )
            db.session.add(new_user)
            db.session.commit()
            auth_token = new_user.generate_auth_token()
            return {
                'status': 'success',
                'message': 'New User Created Successfully',
                'X-API-KEY': auth_token
            }, 201, {'X-API-KEY': auth_token}
        else:
            return {
                'status': 'error',
                'message': 'User already registered!'
            }, 409

    def generate_auth_token(self):
        '''
        Generate authentication token
        :return: string
        '''
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1, seconds=5),
                'iat': datetime.datetime.utcnow(),
                'sub': {
                    "id": self.id,
                    "email": self.email,
                    "is_admin": self.is_admin
                }
            }
            return jwt.encode(payload, secret_key)
        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(token):
        '''
        Decode authentication token
        :param token
        :return integer | string
        '''
        try:
            payload = jwt.decode(token, secret_key, algorithms="HS256")
            return payload['sub']
        except jwt.ExpiredSignatureError:
            return "Expired Token"
        except jwt.InvalidTokenError:
            return "Invalid Token"

    def __repr__(self):
        return f'{self.username}'

from project.extensions import db


class Genre(db.Model):
    __tablename__ = 'genre'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100), unique=True)

    @classmethod
    def create_genre(cls, genre_name):
        genre_name = genre_name.lower()
        genre = cls.query.filter_by(name=genre_name).first()
        if not genre:
            new_genre = Genre(
                name=genre_name
            )
            db.session.add(new_genre)
            db.session.commit()

            return {
                'status': 'success',
                'message': 'new genre added successfully',
                'genre': new_genre
            }, 201
        else:
            return {
                'status': 'error',
                'message': 'Genre already added'
            }, 409

    @classmethod
    def get_or_create_genre(cls, genre_name):
        genre_name = genre_name.lower()
        genre = cls.query.filter_by(name=genre_name).first()
        if not genre:
            return cls.create_genre(genre_name)
        else:
            return {
                'status': 'success',
                'message': 'pre present genre returned',
                'genre': genre
            }, 200

    @classmethod
    def get_all_genres(cls):
        return cls.query.all()
    
    @classmethod
    def get_genre(cls, id):
        return cls.query.get(id)

    def get_movies(self):
        return self.movies.all()    # backref field in Movie.genres

    def save_data(self, data = None):
        data = data if data else self 
        db.session.add(data)
        db.session.commit()

    def __repr__(self):
        return f'{self.name}'

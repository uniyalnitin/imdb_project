import os

from .dev_config import DevlopmentConfig
from .prod_config import ProductionConfig
from .test_config import TestingConfig
from .base_config import Config

get_config = {
    "dev": DevlopmentConfig,
    "prod": ProductionConfig,
    "test": TestingConfig
}

secret_key = Config.SECRET_KEY
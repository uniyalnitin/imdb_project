import unittest
import json
from project.test.base import BaseTestCase
from project.test.test_auth import TestAuthUrl

class TestDirectorUrl(BaseTestCase):

    @staticmethod
    def create_director(obj, headers, parse=False):
        response = obj.client.post('/directors/', data=json.dumps({
            'name': 'Testdirector'
        }),
            content_type="application/json",
            headers = headers
        )
        if parse:
            response_content = json.loads(response.data)
            return response_content, response.status_code
        return response

    def test_create_director(self):
        with self.client:
            response, code = TestAuthUrl.register_admin_user(self, True)
            self.assertTrue(response['X-API-KEY'])

            data, status_code = TestDirectorUrl.create_director(self, {'X-API-KEY': response['X-API-KEY']}, True)
            self.assertEqual(status_code, 201)
            self.assertEqual(data['director']['name'], 'Testdirector')
            


if __name__ == "__main__":
    unittest.main()
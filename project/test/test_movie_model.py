import unittest
from project.test.base import BaseTestCase
from project.flask_app.models.movie import Movie, movie_genre
from project.flask_app.models.genre import Genre
from project.extensions import db

class TestMovieModel(BaseTestCase):

    def test_create_movie(self):
        response, status_code = Movie.create_movie('Kal ho na ho')
        self.assertEqual(status_code, 201)

    def test_create_movie_with_genre_list(self):
        genres = ["horros", "comedy"]
        parse_genre = lambda resp: resp[0]['genre']
        genres = [parse_genre(Genre.get_or_create_genre(name)) for name in genres]
        response, status_code = Movie.create_movie('test movie', 1.2, genres)
        self.assertEqual(response['status'], 'success')

    def test_add_genre(self):
        response, status_code = Movie.create_movie('Kal ho na ho')
        self.assertEqual(status_code, 201)

        movie = response['movie']
        genre_resp, genre_status_code = Genre.create_genre("test")
        self.assertEqual(genre_status_code, 201)

        added_resp, added_status_code = movie.add_genre(genre_resp['genre'])
        movie.save_data()
        self.assertEqual(added_status_code, 203)

    def test_add_or_create_genre(self):
        response, status_code = Movie.create_movie('Kal ho na ho')
        self.assertEqual(status_code, 201)

        movie = response['movie']

        added_resp, added_status_code = movie.add_or_create_genre("string genre")
        movie.save_data()
        self.assertEqual(added_status_code, 201)

if __name__ == "__main__":
    unittest.main()
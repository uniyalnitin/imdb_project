import unittest
import json
from project.test.base import BaseTestCase
from project.test.test_auth import TestAuthUrl

class TestGenreUrl(BaseTestCase):

    @staticmethod
    def create_genre(obj, headers, parse=False):
        response = obj.client.post('/genres/', data=json.dumps({
            'name': 'testGenre'
        }),
            content_type="application/json",
            headers = headers
        )
        if parse:
            response_content = json.loads(response.data)
            return response_content, response.status_code
        return response

    @staticmethod
    def edit_genre(obj, headers, parse=False):
        response = obj.client.put('/genres/1', data=json.dumps({
            'name': 'testGenreChange'
        }),
            content_type="application/json",
            headers = headers
        )
        if parse:
            response_content = json.loads(response.data)
            return response_content, response.status_code
        return response

    def test_create_genre(self):
        with self.client:
            response, code = TestAuthUrl.register_admin_user(self, True)
            self.assertTrue(response['X-API-KEY'])

            data, status_code = TestGenreUrl.create_genre(self, {'X-API-KEY': response['X-API-KEY']}, True)
            self.assertEqual(status_code, 201)

    def test_edit_genre(self):
        with self.client:
            response, code = TestAuthUrl.register_admin_user(self, True)
            self.assertTrue(response['X-API-KEY'])

            data, status_code = TestGenreUrl.create_genre(self, {'X-API-KEY': response['X-API-KEY']}, True)
            self.assertEqual(status_code, 201)

            data, status_code = TestGenreUrl.edit_genre(self, {'X-API-KEY': response['X-API-KEY']}, True)
            self.assertEqual(status_code, 203)



if __name__ == "__main__":
    unittest.main()
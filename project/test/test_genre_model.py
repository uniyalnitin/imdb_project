import unittest
from project.test.base import BaseTestCase
from project.flask_app.models.genre import Genre

class TestGenreModel(BaseTestCase):

    def test_create_genre(self):
        response, status_code = Genre.create_genre("test")
        self.assertEqual(status_code, 201)
        self.assertEqual(response.get('status', 'error'), 'success')

if __name__ == "__main__":
    unittest.main()
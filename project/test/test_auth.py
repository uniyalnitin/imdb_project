import unittest
import json
from project.test.base import BaseTestCase


class TestAuthUrl(BaseTestCase):

    @staticmethod
    def register_admin_user(obj, parse=False):
        response = obj.client.post('/user/', data=json.dumps({
            'email': 'test@gmail.com',
            'username': 'testuser',
            'password': 'testpassword',
            'is_admin': True
        }),
            content_type='application/json'
        )

        if parse:
            response_content = json.loads(response.data)
            return response_content, response.status_code
        return response

    @staticmethod
    def register_user(obj):
        return obj.client.post('/user/', data=json.dumps({
            'email': 'test@gmail.com',
            'username': 'testuser',
            'password': 'testpassword'
        }),
            content_type='application/json'
        )

    @staticmethod
    def login_user(obj, parse=False):
        response = obj.client.post('/auth/login', data=json.dumps({
            'email': 'test@gmail.com',
            'password': 'testpassword'
        }),
            content_type="application/json"
        )
        if parse:
            response_content = json.loads(response.data)
            return response_content, response.status_code
        return response

    def test_registered_user_login(self):
        with self.client:
            response = TestAuthUrl.register_user(self)
            response_content = json.loads(response.data)
            self.assertTrue(response_content['X-API-KEY'])
            self.assertEqual(response.status_code, 201)

            login_response = TestAuthUrl.login_user(self)
            data = json.loads(login_response.data)

            self.assertTrue(data['X-API-KEY'])
            self.assertEqual(login_response.status_code, 200)


if __name__ == "__main__":
    unittest.main()
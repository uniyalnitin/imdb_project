import unittest
import datetime

from project.extensions import db 
from project.flask_app.models.user import User 
from project.test.base import BaseTestCase

class TestUserModel(BaseTestCase):
    def test_generate_auth_token(self):
        user = User(
            id=1,
            email= 'nitin@gmail.com',
            password= "nitin@21",
            username= "testuser",
            is_admin=False,
            created_at= datetime.datetime.utcnow()
        )
        db.session.add(user)
        db.session.commit()
        auth_token = user.generate_auth_token()
        self.assertTrue(isinstance(auth_token, str))
    
    def test_decode_auth_token(self):
        user = User(
            id=1,
            email= 'nitin@gmail.com',
            password= "nitin@21",
            username= "testuser",
            is_admin=False,
            created_at= datetime.datetime.utcnow()
        )
        db.session.add(user)
        db.session.commit()
        auth_token = user.generate_auth_token()
        self.assertTrue(isinstance(auth_token, str))
        self.assertTrue(User.decode_auth_token(auth_token)["id"] == 1)

if __name__ == "__main__":
    unittest.main()
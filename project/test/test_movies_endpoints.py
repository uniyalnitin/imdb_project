import unittest
import json
from project.test.base import BaseTestCase
from project.test.test_auth import TestAuthUrl

class TestMovieUrl(BaseTestCase):

    @staticmethod
    def get_movies(obj, headers, parse=False):
        response = obj.client.get('/movies/', headers=headers)
        if parse:
            response_content = json.loads(response.data)
            return response_content, response.status_code
        return response

    @staticmethod
    def create_movie(obj, headers ,parse=False):
        response = obj.client.post('/movies/', data=json.dumps({
            'name': 'changedName',
            'imdb_score': 3.5,
            "genres": ["romance"]
        }),
            content_type="application/json",
            headers = headers
        )
        if response:
            response_content = json.loads(response.data)
            return response_content, response.status_code
        return response

    @staticmethod
    def get_movie_detail(obj, headers, parse=False):
        response = obj.client.get('/movies/1', headers=headers)
        if parse:
            response_content = json.loads(response.data)
            return response_content, response.status_code
        return response

    @staticmethod 
    def edit_movie(obj, headers, parse=False):
        response = obj.client.put('/movies/1', data=json.dumps({
            'name': 'testMovie',
            'imdb_score': 2.5,
            "genres": ["horror", "comedy"]
        }),
            content_type="application/json",
            headers = headers
        )
        if response:
            response_content = json.loads(response.data)
            return response_content, response.status_code
        return response

    def test_get_movies(self):
        with self.client:
            response, code = TestAuthUrl.register_admin_user(self, True)
            self.assertTrue(response['X-API-KEY'])

            data, status_code = TestMovieUrl.get_movies(self, {'X-API-KEY': response['X-API-KEY']}, True)
            self.assertEqual(status_code, 200)
            self.assertEqual(data['data'], [])

    def test_create_movie(self):
        with self.client:
            response, code = TestAuthUrl.register_admin_user(self, True)
            self.assertTrue(response['X-API-KEY'])
            data, status_code = TestMovieUrl.create_movie(self, {'X-API-KEY': response['X-API-KEY']}, True)
            self.assertEqual(status_code, 201)

    def test_get_movie_detail(self):
        with self.client:
            response, code = TestAuthUrl.register_admin_user(self, True)
            self.assertTrue(response['X-API-KEY'])
            
            data, status_code = TestMovieUrl.create_movie(self, {'X-API-KEY': response['X-API-KEY']}, True)
            self.assertEqual(status_code, 201)

            data, status_code = TestMovieUrl.get_movie_detail(self, {}, True)
            self.assertEqual(status_code, 200)

    def test_edit_movie(self):
        with self.client:
            response, code = TestAuthUrl.register_admin_user(self, True)
            self.assertTrue(response['X-API-KEY'])
            
            data, status_code = TestMovieUrl.create_movie(self, {'X-API-KEY': response['X-API-KEY']}, True)
            self.assertEqual(status_code, 201)

            data, status_code = TestMovieUrl.edit_movie(self, {'X-API-KEY': response['X-API-KEY']}, True)
            self.assertEqual(status_code, 203)

if __name__ == "__main__":
    unittest.main()
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt

flask_bcrypt = Bcrypt()
db = SQLAlchemy()
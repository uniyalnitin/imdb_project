from flask import Flask, Blueprint
from flask_sqlalchemy import SQLAlchemy
from project.config.main import get_config
from project.extensions import db, flask_bcrypt

def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(get_config[config_name])
    register_extensions(app)
    return app

def register_extensions(app):
    db.init_app(app)
    flask_bcrypt.init_app(app)